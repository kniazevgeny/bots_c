﻿namespace SkBot
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MessageReadTimer = new System.Windows.Forms.Timer(this.components);
            this.loginButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.mainTokenTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // MessageReadTimer
            // 
            this.MessageReadTimer.Interval = 10000;
            this.MessageReadTimer.Tick += new System.EventHandler(this.MessageReadTimer_Tick);
            // 
            // loginButton
            // 
            this.loginButton.Location = new System.Drawing.Point(11, 11);
            this.loginButton.Margin = new System.Windows.Forms.Padding(2);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(178, 24);
            this.loginButton.TabIndex = 2;
            this.loginButton.Text = "Login";
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // AllBotDataSet1
            // 
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(67, 37);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Main Token";
            // 
            // mainTokenTextBox
            // 
            this.mainTokenTextBox.Location = new System.Drawing.Point(11, 52);
            this.mainTokenTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.mainTokenTextBox.Name = "mainTokenTextBox";
            this.mainTokenTextBox.Size = new System.Drawing.Size(178, 20);
            this.mainTokenTextBox.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(203, 93);
            this.Controls.Add(this.mainTokenTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.loginButton);
            this.Name = "Form1";
            this.Text = "SkBot";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer MessageReadTimer;
        private System.Windows.Forms.Button loginButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox mainTokenTextBox;
    }
}

