﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Categories;
using VkNet.Enums.Filters;
using VkNet.Utils;
using VkNet.Utils.AntiCaptcha;
using VkNet.Model.RequestParams;
using SkBot.utilities;
using System.Text.RegularExpressions;
using System.Globalization;

namespace SkBot
{
    class Users : MessageProcessor
    {

        public Users(ref VkApiWithQuery _vk) : base(ref _vk)
        {
            this.addCommand("Команды", commListCommand, new List<String> { "Команда", "Список команд", "К", "R" }, "выводит список доступных команд");
        }

        public override bool processMessage(VkNet.Model.Message message)
        {
            if (!processCommand(message))
            {
                vkapi.sendMessageToQuery(new MessagesSendParams
                {
                    UserId = message.UserId,
                    Message =  "Такой команды не существует"
                });
            }
            else
                return true;


            return false;
        }

        public void commListCommand(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            StringBuilder str = new StringBuilder("Вы можете воспользоваться этими командами. Для этого напишите мне команду или первую букву команды:\r\n\r\n");
            foreach (String name in this.getCommands()) str.AppendLine(name);
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.UserId,
                Message = str.ToString(),
            });
            messageThread.fsm_state = 0;
        }
    }
}