﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Categories;
using VkNet.Enums.Filters;
using VkNet.Utils;
using VkNet.Utils.AntiCaptcha;
using VkNet.Model.RequestParams;
using SkBot.utilities;
using System.Drawing;
using System.Globalization;
using OfficeOpenXml;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Text.RegularExpressions;

namespace SkBot
{
    class Bots
    {
        VkApiWithQuery mainVK;
        List<Mycommand> commands_list = new List<Mycommand>();
        List<String> savedPhotos = new List<String>();
        List<Messagethread> thread_list = new List<Messagethread>();
        //ImageProcessing img;
        public Bots(ref VkApiWithQuery _mainVK)
        {
            mainVK = _mainVK;
            commands_list.Add(new Mycommand("Привет", helloCommand));
            commands_list.Add(new Mycommand("Начать", Salution));
            commands_list.Add(new Mycommand("Что ты умеешь", BOTcmds));
            commands_list.Add(new Mycommand("Команды", BOTcmds));
            commands_list.Add(new Mycommand("Пока", goodbye));
            commands_list.Add(new Mycommand("Ф", getphoto));
            //Вот это все доступные команды
        }

        private void helloCommand(VkNet.Model.Message message, Messagethread thread)
        {
            var user = mainVK.vkApi.Users.Get(new long[] { (long)message.UserId });
            mainVK.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.UserId,
                Message = "Привет, " + user[0].FirstName
            });
        }
        private void BOTcmds(VkNet.Model.Message message, Messagethread thread)
        {
            mainVK.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.UserId,
                Message = "Пока что мой создатель запустил только одну команду обработки фото конспектов. " +
                "Для того, чтобы вызвать её напишите Фото(ф)"
            });
        }
        private void Salution(VkNet.Model.Message message, Messagethread thread)
        {
            var user = mainVK.vkApi.Users.Get(new long[] { (long)message.UserId });
            Analyse inf = new Analyse(message.UserId, mainVK);
            inf.Analysing(message.UserId);
            mainVK.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.UserId,
                Message = "Привет! Меня зовут Mr. Фотограф и я умею обрабатывать изображения. Изначально я был создан для того, чтобы обрабатывать" +
                " фото конспектов/текстов, и делать их чётче. Я использую алгеритм k-means или k-средних, чтобы понять, " +
                "где какие цвета. \r\n\r\n" +
                "Перед тем, как закинуть фотографию, напишите Фото(ф) и следуйте инструкциям." +
                "Чтобы узнать все команды напишите 'Команды'"
            });
        }
        private void goodbye(VkNet.Model.Message message, Messagethread thread)
        {
            mainVK.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.UserId,
                Message = "Пока"
            });
        }
        private void getphoto(VkNet.Model.Message message, Messagethread thread)
        {
            if (thread.fsm_state == 0)
            {
                mainVK.sendMessageToQuery(new MessagesSendParams
                {
                    UserId = message.UserId,
                    Message = "Теперь пришли мне фотку"
                });
                thread.fsm_state = 1;
                return;
            }
            if (thread.fsm_state == 1) {
                if (message.Attachments.Count == 0) {
                    mainVK.sendMessageToQuery(new MessagesSendParams
                    {
                        UserId = message.UserId,
                        Message = "Мне нужно фото"
                    });
                    return;
                }
                var count = message.Attachments.Count;
                String photoSize = "";
                Regex r = new Regex("\\d+");
                Match m = r.Match(message.Body);
                List<String> usedTOdelete = new List<String>();
                int k = 6;
                if (m.Success) { k = Convert.ToInt32(m.Value); }
                if (k < 2)
                {
                    mainVK.sendMessageToQuery(new MessagesSendParams
                    {
                        UserId = message.UserId,
                        Message = "0шибка! Параметр не может быть меньше 2"
                    });
                    return;
                }
                if (k > 25)
                {
                    mainVK.sendMessageToQuery(new MessagesSendParams
                    {
                        UserId = message.UserId,
                        Message = "0шибка! Параметр не может быть больше 25"
                    });
                    return;
                }
                mainVK.vkApi.Messages.Send(new MessagesSendParams
                {
                    UserId = message.UserId,
                    Message = "Сейчас-сейчас..."
                });
                List<String> readyFiles = new List<String>();
                for (int i = 0; i < count; i++)
                {
                    if (message.Attachments[i].Type.Name == "Photo") {
                        if ((message.Attachments[i].Instance as VkNet.Model.Attachments.Photo).Photo75 != null) photoSize = "Photo75";
                        if ((message.Attachments[i].Instance as VkNet.Model.Attachments.Photo).Photo130 != null) photoSize = "Photo130";
                        if ((message.Attachments[i].Instance as VkNet.Model.Attachments.Photo).Photo604 != null) photoSize = "Photo604";
                        if ((message.Attachments[i].Instance as VkNet.Model.Attachments.Photo).Photo807 != null) photoSize = "Photo807";
                        if ((message.Attachments[i].Instance as VkNet.Model.Attachments.Photo).Photo1280 != null) photoSize = "Photo1280";
                        if ((message.Attachments[i].Instance as VkNet.Model.Attachments.Photo).Photo2560 != null) photoSize = "Photo2560";
                        //ищем фото лучшего качества
                        savecurrentPhoto(message, photoSize, i, thread);
                        //Сохраняем его
                        //String urlC = Contrast(savedPhotos[i]);
                        ImageProcessing img = new ImageProcessing(savedPhotos[i], k);
                        readyFiles.Add(img.runKMeans(k));
                        //Обрабатываем
                        String del = savedPhotos[i] + ".png";
                        usedTOdelete.Add(del);
                        usedTOdelete.Add(readyFiles[i]);
                        //Добавляем пути в список к удалению
                    }
                    else
                    {
                        mainVK.sendMessageToQuery(new MessagesSendParams
                        {
                            UserId = message.UserId,
                            Message = "Мне нужно фото"
                        });
                        return;
                    }

                }
                List<VkNet.Model.Attachments.Photo> photo = SendPhoto(readyFiles);
                mainVK.sendMessageToQuery(new MessagesSendParams
                {
                    Attachments = photo,
                    UserId = message.UserId,
                    Message = "Готов0!"
                });
                //Отправляем файлы
                deleteUsedFiles(usedTOdelete);
                readyFiles.Clear();
                savedPhotos.Clear();
                //Удаляем ненужное
                thread.fsm_state = 0;
                return;
            }
            return;
        }
        private bool savecurrentPhoto(VkNet.Model.Message message, String photoSize, int i, Messagethread thread)
        {
            using (WebClient webClient = new WebClient())
            {
                String dir = Directory.GetCurrentDirectory();
                //Смотрим, где мы есть
                //И сохраняем фотку, которую нам прислали
                if (photoSize == "Photo75")
                {
                    webClient.DownloadFile((message.Attachments[i].Instance as VkNet.Model.Attachments.Photo).Photo75.AbsoluteUri,
                        dir + "\\" + message.Attachments[i].Instance + ".png");
                    savedPhotos.Add(dir + "\\" + message.Attachments[i].Instance);
                    return true;
                }
                else if (photoSize == "Photo130")
                {
                    webClient.DownloadFile((message.Attachments[i].Instance as VkNet.Model.Attachments.Photo).Photo130.AbsoluteUri,
                        dir + "\\" + message.Attachments[i].Instance + ".png");
                    savedPhotos.Add(dir + "\\" + message.Attachments[i].Instance);
                    return true;
                }
                else if (photoSize == "Photo604")
                {
                    webClient.DownloadFile((message.Attachments[i].Instance as VkNet.Model.Attachments.Photo).Photo604.AbsoluteUri,
                        dir + "\\" +  message.Attachments[i].Instance + ".png");
                    savedPhotos.Add(dir + "\\" + message.Attachments[i].Instance);
                    return true;
                }
                else if (photoSize == "Photo807")
                {
                    webClient.DownloadFile((message.Attachments[i].Instance as VkNet.Model.Attachments.Photo).Photo807.AbsoluteUri,
                        dir + "\\" + message.Attachments[i].Instance + ".png");
                    savedPhotos.Add(dir + "\\" + message.Attachments[i].Instance);
                    return true;
                }
                else if (photoSize == "Photo1280")
                {
                    webClient.DownloadFile((message.Attachments[i].Instance as VkNet.Model.Attachments.Photo).Photo1280.AbsoluteUri,
                        dir + "\\" + message.Attachments[i].Instance + ".png");
                    savedPhotos.Add(dir + "\\" + message.Attachments[i].Instance);
                    return true;
                }
                else if (photoSize == "Photo2560")
                {
                    webClient.DownloadFile((message.Attachments[i].Instance as VkNet.Model.Attachments.Photo).Photo2560.AbsoluteUri,
                        dir + "\\" + message.Attachments[i].Instance + ".png");
                    savedPhotos.Add(dir + "\\" + message.Attachments[i].Instance);
                    return true;
                }
                else return false;
            }
        }
        private List<VkNet.Model.Attachments.Photo> SendPhoto(List<String> files)
        {
            //Адрес сервера для загрузки
            var uploadServer = mainVK.vkApi.Photo.GetMessagesUploadServer(123);
            //Загружаем файл
            var WC = new WebClient();
            List<String> Photos = new List<String>();
            var photoes = new List<VkNet.Model.Attachments.Photo>();
            foreach (var file in files)
            {
                var responseFile = Encoding.ASCII.GetString(WC.UploadFile(uploadServer.UploadUrl, file));
                photoes.Add(mainVK.vkApi.Photo.SaveMessagesPhoto(responseFile)[0]);
            }
            return photoes;
        }

        private bool deleteUsedFiles(List<String> paths)
        {
            foreach (var path in paths)
            {  
                File.Delete(path);
            }
            return true;
        }

        public bool processMessage(VkNet.Model.Message message)
        {
            Messagethread thread = thread_list.Find(x => x.userid == message.UserId);
            if (thread == null)
            {
                thread_list.Add(new Messagethread(message.UserId, "", 0));
                foreach (var command in commands_list)
                {
                    if (command.isCommand(message, thread_list.Last()))
                    {
                        if (thread_list.Last().fsm_state == 0) thread_list.Remove(thread_list.Last());
                        return true;
                    }
                }
                thread_list.Remove(thread_list.Last());
            }
            else
            {
                Mycommand comm = commands_list.Find(x => x.commandname.Equals((thread.commandname), StringComparison.CurrentCultureIgnoreCase));
                comm.callback(message, thread);
                if (thread.fsm_state == 0) thread_list.Remove(thread);
                return true;
            }
            mainVK.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.UserId,
                Message = "Прости, я тебя не понял.\r\n" + 
                "Для того, чтобы я тебя понял правильно, введи 'Команды' и узнай, что я умею"
            });
            return false;
        }
    }
}
