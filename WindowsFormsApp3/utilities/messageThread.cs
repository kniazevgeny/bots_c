﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkBot.utilities
{
    public class Messagethread
    {
        public long? userid { get; set; }
        public string commandname { get; set; }
        public int fsm_state { get; set; }
        public object arg { get; set; }

        public Messagethread(long? _userid, string _commandname, int _fsm_state, object _arg)
        {
            userid = _userid;
            commandname = _commandname;
            fsm_state = _fsm_state;
            arg = _arg;
        }
        public Messagethread(long? _userid, string _commandname, int _fsm_state)
        {
            userid = _userid;
            commandname = _commandname;
            fsm_state = _fsm_state;
        }
    }
}
