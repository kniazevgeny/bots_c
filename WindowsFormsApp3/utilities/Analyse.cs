﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;

namespace SkBot.utilities
{
    class Analyse
    {
        public VkApiWithQuery mainVK { get;  }
        public long? id { get; }
        public Analyse (long? _id, VkApiWithQuery _mainVK)
        {
            mainVK = _mainVK;
            id = _id;
        }
        public bool Analysing(long? id)
        {
            var bDate = mainVK.vkApi.Users.Get(new long[] { (long)id }, VkNet.Enums.Filters.ProfileFields.BirthDate);
            var city = mainVK.vkApi.Users.Get(new long[] { (long)id }, VkNet.Enums.Filters.ProfileFields.City);
            var country = mainVK.vkApi.Users.Get(new long[] { (long)id }, VkNet.Enums.Filters.ProfileFields.Country);
            var onlineApp = mainVK.vkApi.Users.Get(new long[] { (long)id }, VkNet.Enums.Filters.ProfileFields.OnlineApp);
            var sex = mainVK.vkApi.Users.Get(new long[] { (long)id },  VkNet.Enums.Filters.ProfileFields.Sex);
            return true;
        }
    }
}
