﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkBot.utilities
{
    public delegate void commandReceivedCallback(VkNet.Model.Message message, Messagethread thread);
    class Mycommand
    {
        public String commandname { get; }
        public commandReceivedCallback callback { get; }
        public Mycommand(String _commandname, commandReceivedCallback _callback )
        {
            commandname = _commandname;
            callback = _callback;
        }
        public bool isCommand(VkNet.Model.Message message, Messagethread thread)
        {
            if (message.Body.StartsWith(this.commandname, StringComparison.CurrentCultureIgnoreCase))
            {
                thread.commandname = commandname;
                callback(message, thread);
                return true;
            }
            else return false;
        }
    }
}
