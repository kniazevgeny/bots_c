﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Categories;
using VkNet.Enums.Filters;
using VkNet.Utils;
using VkNet.Utils.AntiCaptcha;
using VkNet.Model.RequestParams;
using System.ComponentModel;
using WindowsFormsApp3.utilities;

namespace WindowsFormsApp3
{
    class quiz : messageprocessor
    {
        class question
        {
            public String questionName { get; }
            public List<String> answers { get; }
            public bool isAnswered;
            public bool isPosted;
            public uint timer = 60000;
            public question(String _question, List<String> _answers)
            {
                questionName = _question;
                answers = _answers;
                isAnswered = false;
                isPosted = false;
            }
            public question(String _question, List<String> _answers, uint _timer)
            {
                questionName = _question;
                answers = _answers;
                isAnswered = false;
                isPosted = false;
                timer = _timer;
            }
            public void setTimer(uint newTimer)
            {
                timer = newTimer;
            }
            public bool isRightAnswer(String message)
            {
                if (answers != null)
                {
                    if (answers.Exists(x => x.Equals(message, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        return true;
                    }
                }
                return false;
            }
        }
        class player
        {
            public long userID;
            public String firstName;
            public String lastName;
            public int score;
            public player(long _userID, String fName, String lName)
            {
                userID = _userID;
                firstName = fName;
                lastName = lName;
                score = 0;
            }
        }
        bool isActive = false;
        bool isStarted = false;
        long chatId;
        List<player> players = new List<player>();
        List<question> quizQuestions = new List<question>();
        int currentQuestion = 0;
        private System.ComponentModel.BackgroundWorker backgroundQuiz;

        public quiz(ref VkApiWithQuery _vk, long _chat_id) : base(ref _vk)
        {
            chatId = _chat_id;
            backgroundQuiz = new System.ComponentModel.BackgroundWorker();
            backgroundQuiz.WorkerSupportsCancellation = true;
            backgroundQuiz.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
            backgroundQuiz.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
        }

        public override bool processMessage(VkNet.Model.Message message)
        {
            if (message.ReadState == VkNet.Enums.MessageReadState.Unreaded)
            {
                if (message.ChatId == chatId)
                {
                    if (message.UserId != message.AdminId)
                    {
                        if ((isStarted == true) && (quizQuestions.Count != 0))
                        {
                            if ((quizQuestions[currentQuestion].isRightAnswer(message.Body)) && (quizQuestions[currentQuestion].isAnswered == false))
                            {
                                if (currentQuestion < quizQuestions.Count)
                                {
                                    quizQuestions[currentQuestion].isAnswered = true;
                                    player pl = players.Find(x => x.userID == message.UserId);
                                    pl.score = pl.score + 1;
                                    vkapi.sendMessageToQuery(new MessagesSendParams
                                    {
                                        ChatId = chatId,
                                        Message = "Игрок " + pl.firstName + " " + pl.lastName + " дал правильный ответ \"" + message.Body
                                        + "\" и увеличивает счет до " + pl.score.ToString() + " очков",
                                    });
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        public void addQuestion(String _question, List<String> _answers, uint _timer)
        {
            quizQuestions.Add(new question(_question, _answers, _timer));
            isActive = true;
        }

        public void addQuestion(String _question, List<String> _answers)
        {
            quizQuestions.Add(new question(_question, _answers));
            isActive = true;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            System.Threading.Thread.Sleep(5000);

            while (currentQuestion < quizQuestions.Count)
            {
                if (quizQuestions[currentQuestion].isPosted == false)
                {
                    vkapi.sendMessageToQuery(new MessagesSendParams
                    {
                        ChatId = chatId,
                        Message = quizQuestions[currentQuestion].questionName,
                    });
                    quizQuestions[currentQuestion].isPosted = true;
                }
                if (quizQuestions[currentQuestion].timer > 0)
                {
                    quizQuestions[currentQuestion].timer -= 1000;
                    System.Threading.Thread.Sleep(1000);
                }
                else
                {
                    quizQuestions[currentQuestion].isAnswered = true;
                    vkapi.sendMessageToQuery(new MessagesSendParams
                    {
                        ChatId = chatId,
                        Message = "Правильный ответ \"" + quizQuestions[currentQuestion].answers[0]
                            + "\". Никто из игроков не назвал правильного ответа",
                    });
                }
                if (quizQuestions[currentQuestion].isAnswered == true)
                {
                    currentQuestion++;
                    System.Threading.Thread.Sleep(4000);
                } 
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            isActive = false;
            isStarted = false;
            currentQuestion = 0;
            var scores = players.OrderByDescending(x => x.score);
            StringBuilder sb = new StringBuilder();
            foreach (player pl in scores)
                sb.AppendLine(pl.firstName + " " + pl.lastName + " - " + pl.score + " очков");
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                ChatId = chatId,
                Message = "Вопросы кончились. Всем спасибо. Результаты викторины:\r\n" + sb.ToString(),
            });
            System.Threading.Thread.Sleep(3000);
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                ChatId = chatId,
                Message = "У вас есть ровно 98 секунд, чтобы обсудить викторину и написать свои предложения/жалобы/вопросы, а после вас удалят из беседы. Приятного обсуждения.",
            });
            System.Threading.Thread.Sleep(98000);
            var chatUsers = vkapi.vkApi.Users.Get(vkapi.vkApi.Messages.GetChat(chatId).Users);
            while (chatUsers.Count > 1)
            {
                vkapi.vkApi.Messages.RemoveChatUser(chatId, chatUsers[chatUsers.Count - 1].Id);
                chatUsers = vkapi.vkApi.Users.Get(vkapi.vkApi.Messages.GetChat(chatId).Users);
                System.Threading.Thread.Sleep(3000);
            }
            players.Clear();
            quizQuestions.Clear();
        }

        public void startQuiz()
        {
            isStarted = true;
            currentQuestion = 0;
            var users = vkapi.vkApi.Users.Get(vkapi.vkApi.Messages.GetChat(chatId).Users);
            foreach (var user in users)
                players.Add(new player(user.Id, user.FirstName, user.LastName));
            backgroundQuiz.RunWorkerAsync();
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                ChatId = chatId,
                Message = "Викторина начнется через 5 секунд"
            });
        }

        public bool isQuizActive()
        {
            return isActive;
        }

        public bool isQuizStarted()
        {
            return isStarted;
        }

        public int addPlayer(long userID)
        {
            if (!isActive) return 3;
            if (isStarted) return 2;
            try
            {
                vkapi.vkApi.Messages.AddChatUser(chatId, userID);
            }
            catch (Exception ex)
            {
                return 1;
            }
            return 0;
        }


    }
}
