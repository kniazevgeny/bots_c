﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Accord.Imaging.Converters;
using Accord.MachineLearning;
using Accord.Math;
using Accord.Statistics.Distributions.DensityKernels;
using Accord.Math.Distances;

namespace SkBot.utilities
{
    class ImageProcessing
    {
        public String urltoPhoto { get; }
        public Int32 k { get; }
        public ImageProcessing(String _savedPhotos, Int32 _k)
        {
            urltoPhoto = _savedPhotos;
            k = _k;
        }
        public String gettingPhoto()
        {
            String uri;
            try
            {
                Bitmap FirstStepImage = (Bitmap)Image.FromFile(urltoPhoto + ".png", true);
                var w = FirstStepImage.Width;
                var h = FirstStepImage.Height;
                List<Color> FirstStepPixels = new List<Color>();
                byte MaxR = 0; byte MaxG = 0; byte MaxB = 0;
                byte MinR = 255; byte MinG = 255; byte MinB = 255;
                Color Pixel;
                byte AvR; byte AvG; byte AvB;
                Color RandomColor;
                Random randomize = new Random();  
                for (var x = 0; x < w - 1; x++)
                {
                    for (var y = 0; y < h - 1; y++)
                    {
                        Pixel = FirstStepImage.GetPixel(x, y);
                        FirstStepPixels.Add(Pixel);
                        if (Pixel.R > MaxR) MaxR = Pixel.R;
                        if (Pixel.R < MinR) MinR = Pixel.R;
                        if (Pixel.G > MaxG) MaxG = Pixel.G;
                        if (Pixel.G < MinG) MinG = Pixel.G;
                        if (Pixel.B > MaxB) MaxB = Pixel.B;
                        if (Pixel.B < MinB) MinB = Pixel.B;
                    }

                }
                //var light = FirstStepImage;
                for (var he = 0; he < h; he = he + (h / 200))
                {
                    for (var wi = 0; wi < w; wi = wi + (w / 200))
                    {

                        //MaxR = MiMax(MaxR, true); MinR = MiMax(MinR, false);
                        //MaxR = MiMax(MaxG, true); MinG = MiMax(MinG, false);
                        //MaxR = MiMax(MaxB, true); MinB = MiMax(MinB, false);

                        int myrandomintR = randomize.Next(MinR, MaxR);
                        int myrandomintG = randomize.Next(MinG, MaxG);
                        int myrandomintB = randomize.Next(MinB, MaxB);

                        RandomColor = Color.FromArgb(myrandomintR, myrandomintG, myrandomintB);
                        FirstStepImage.SetPixel(wi, he, RandomColor);
                    }
                }
                FirstStepImage.Save(urltoPhoto + "__File for user_" + w + ".png");
                uri = urltoPhoto + "__File for user_" + w + ".png";
                var Palette = FirstStepImage.Palette;
            }
            catch (System.IO.FileNotFoundException) { return "error with ImageProcessing"; }
            return uri;
        }
        private byte MiMax(byte color, bool mode)
        {
            if (mode)
            {
                if (255 - color >= 16)
                {
                    color += 15;
                }
            }
            else
            {
                if (0 + color >= 16)
                {
                    color -= 15;
                }
            }
            return color;
        }
        /*public String kMeans()
        {
            Bitmap image = (Bitmap)Image.FromFile(urltoPhoto + ".png", true);
            int pixelSize = 6;
            double sigma = (double)numBandwidth.Value;
            ImageToArray imageToArray = new ImageToArray(min: -1, max: +1);
            ArrayToImage arrayToImage = new ArrayToImage(image.Width, image.Height, min: -1, max: +1);
            double[][] pixels; imageToArray.Convert(image, out pixels);
            var meanShift = new MeanShift()
            {
                Kernel = new GaussianKernel(pixelSize),
                Bandwidth = sigma,
            };
            int[] idx = meanShift.Learn(pixels).Decide(pixels);
            pixels.Apply((x, i) => meanShift.Clusters.Modes[idx[i]], result: pixels);
            Bitmap result; arrayToImage.Convert(pixels, out result);
            result.Save(urltoPhoto + "__File for user_" + w + ".png");
            var uri = urltoPhoto + "__File for user_" + ".png";
            return "ok";
        }*/
        public String runKMeans(Int32 k)
        {
            Bitmap image = Accord.Imaging.Image.FromFile(urltoPhoto + ".png");

            ImageToArray imageToArray = new ImageToArray(min: -1, max: +1);
            ArrayToImage arrayToImage = new ArrayToImage(image.Width, image.Height, min: -1, max: +1);
            double[][] pixels; imageToArray.Convert(image, out pixels);
            KMeans kmeans = new KMeans(k, new SquareEuclidean())
            {
                Tolerance = 0.05
            };
            int[] idx = kmeans.Learn(pixels).Decide(pixels);
            pixels.Apply((x, i) => kmeans.Clusters.Centroids[idx[i]], result: pixels);
            Bitmap result; arrayToImage.Convert(pixels, out result);
            result.Save(urltoPhoto + "__File for user_" + ".png");
            String uri = urltoPhoto + "__File for user_" + ".png";
            return uri;
        }
    }
}

